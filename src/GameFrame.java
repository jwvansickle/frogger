import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/* John VanSickle
 * This is the primary class, containing the GUI components and all listeners.
 */

public class GameFrame extends JFrame {
	
	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 616;
	public static final int HEIGHT = 410;
	//variables
	private JButton startButton;
	private JButton stopButton;
	private JButton fasterButton;
	private JButton slowerButton;
	private JLabel livesDisplay;
	private JPanel buttonPanel;
	private GamePanel myGamePanel;
	private Thread drawingThread;
	private Integer lives;

	public GameFrame() {
		//initialize the DrawFrame
		super("Java Drawings - John VanSickle");
		this.setLayout(new BorderLayout());
		this.setSize(WIDTH, HEIGHT);
		
		//initialize buttons and add them to button panel
		InitializeButtonPanel();
		//add buttonPanel to the DrawFrame
		this.add(buttonPanel, BorderLayout.NORTH);
		
		//intialize the GamePanel and add it to the GameFrame
		myGamePanel = new GamePanel(this);
		this.add(myGamePanel, BorderLayout.CENTER);
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	//InitializeButtonPanel() will instantiate buttons and add them to the buttonPanel, and then the buttonPanel can be added to the GameFrame
	public void InitializeButtonPanel(){
		//instantiate the buttonPanel
		buttonPanel = new JPanel();
		
		//instantiate number of lives
		lives =4;
		
		//instantiate buttons
		startButton = new JButton("Start");
		stopButton = new JButton("Pause (P)");
		fasterButton = new JButton("Difficulty +");
		slowerButton = new JButton("Difficulty -");
		livesDisplay = new JLabel("Lives: " + lives);
		
		//add listener to start and stop buttons
		startStopListener mySSListener = new startStopListener();
		startButton.addActionListener(mySSListener);
		stopButton.addActionListener(mySSListener);
		
		//add listener to fast slow buttons
		fastSlowListener myFSListener = new fastSlowListener();
		fasterButton.addActionListener(myFSListener);
		slowerButton.addActionListener(myFSListener);
		
		//add buttons to buttonPanel
		buttonPanel.add(startButton);
		buttonPanel.add(stopButton);
		buttonPanel.add(fasterButton);
		buttonPanel.add(slowerButton);
		buttonPanel.add(livesDisplay);
	}//end of InitializeButtons()
	
	//function for the GamePanel to be able to re-enable buttons to "Restart" the game
	public void enableButtons(){
		startButton.setEnabled(true);
		stopButton.setEnabled(true);
	}//end of enable buttons
	
	public void resetLives(){
		lives = 4;
	}
	
	public void decLives(){
		lives--;
	}
	
	public int getLives(){
		return lives;
	}
	
	public void updateLivesDisplay(){
		livesDisplay.setText("Lives: " + lives);
	}
	
	//inner class for the buttons to be able to be clicked to continue or pause the game
	private class startStopListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == startButton)
			{

				myGamePanel.startGame();
				
				startGamePanelThread();
				//Disable the start button
				startButton.setEnabled(false);
			}
			else if (e.getSource() == stopButton)
			{
				myGamePanel.pauseGame();
				//Enable the start button
				startButton.setEnabled(true);
			}

				
		}
	}
	
	//use thread for the GamePanel so that the buttons of the GUI and panel work simultaneously
	public void startGamePanelThread(){
		//Create a new thread in which is the drawing Panel
		drawingThread = new Thread(myGamePanel);
		//In order to tell the thread to run its run method, call the start method
		drawingThread.start();
	}
	
	//inner class for the buttons to be able to speed up or slow down the game (harder and easier)
	private class fastSlowListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == fasterButton){
				if (myGamePanel.getSleepTime() >= 10)
					myGamePanel.setSleepTime(myGamePanel.getSleepTime() - 10);
			}
			else{
				myGamePanel.setSleepTime(myGamePanel.getSleepTime() + 10);
			}
		}		
	}

	public static void main(String[] args) {
		@SuppressWarnings("unused")
		GameFrame myGameFrame = new GameFrame();
	}
}
