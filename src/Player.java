import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/* John VanSickle Player class
 * It has added functionality to have a current angle which allows the frog to appear that it turns when moving
 */

public class Player extends ImageEntity{
	
	private double currentAngle;

	public Player(int pX, int pY, int pVelX, int pVelY, String pFileName, boolean pIsActive, boolean pIsVisible)
	{
		super(pX, pY, pVelX, pVelY, pFileName, pIsActive, pIsVisible);
		at = new AffineTransform();
		currentAngle = 0;
	}

	public void updateX(int pVal)
	{
		super.setLocX(super.getLocX()+pVal);
	}
	
	
	public void updateY(int pVal)
	{
		super.setLocY(super.getLocY()+pVal);
	}

	@Override
	public void draw(Graphics g) {
		//transform
		Graphics2D g2d = (Graphics2D)g;
		at.setToIdentity();
		at.translate(getLocX(), getLocY());
		at.translate(this.getImage().getWidth(null)/2.0,
		this.getImage().getHeight(null)/2.0);
		at.rotate(Math.toRadians(currentAngle));
		at.translate(-(this.getImage().getWidth(null)/2.0),
				-(this.getImage().getHeight(null)/2.0));
		
		//actually draw
		g2d.drawImage(getImage(),  at,  null);
	}
	
	public void setCurrentAngle(double pAngle){
		currentAngle = pAngle;
	}
	
	//Override
	@Override
	public void updateSprite()
	{
		setLocX(getLocX() + getVelX());
		setLocY(getLocY() + getVelY());
	}
	
	//*****************************************************

		

}












