/* John VanSickle
 * The Enemy class extends ImageEntity which extends sprite. 
 * This gives Enemy the capability to have "AI" and to move freely by using its 
 * inherited updateSprite method and each Enemy object will have its own Velocity and 
 * lane of traffic
 */

public class Enemy  extends ImageEntity{
	
	private boolean drivesRight;

	public Enemy(int pX, int pY, int pVelX, int pVelY, String pFileName, boolean pIsActive, boolean pIsVisible, boolean pDrivesRight)
	{
		super(pX, pY, pVelX, pVelY, pFileName, pIsActive, pIsVisible);
		drivesRight = pDrivesRight;
	}

	//commented out because I don't want the Enemies to be updated in this direct way - 
	//will be updated through updateSprite method below
	public void updateX(int pVal)
	{
		//super.setLocX(super.getLocX()+pVal);
	}
	public void updateY(int pVal)
	{
		//super.setLocY(super.getLocY()+pVal);
	}
	
	public boolean isDriveRight(){
		return drivesRight;
	}
	
	@Override
	public void updateSprite(){
		if (!drivesRight && getLocX() + 100 <0 ){
			setLocX(GamePanel.WIDTH_OF_GAMEPANEL_BACKGROUND);
		}
		if ( drivesRight && getLocX() > GamePanel.WIDTH_OF_GAMEPANEL_BACKGROUND)
		{
			//new idea of wrapping the enemy
			setLocX(0 - 100);
		}
		setLocX(getLocX() + getVelX());
		setLocY(getLocY() + getVelY());
	}//end of updateSprite
	
}
