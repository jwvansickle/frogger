/***************************************************
 * Sprite Class -An abstract class which defines
 * the characteristics of objects that will be used
 * inside of our games and simulations.  More will be 
 * added to this
 * John VanSickle
 ***************************************************/

import java.awt.*;

public abstract class Sprite {
	
	//x and y location of the object on the screen
	protected int locX, locY;
	//velocity of the sprite
	protected int velX, velY;
	
	protected Image img; //Image of the Sprite
	protected boolean visible; //is the sprite visible
	protected boolean active; // is the sprite update-able
	
	public Sprite()
	{	
		//no default constructor because of necessity for the image
	}
	
	
	public Sprite(int pX, int pY, int pVelX, int pVelY, boolean pVisible, boolean pActive, Image pImage)
	{
		locX = pX;
		locY = pY;
		velX = pVelX;
		velY = pVelY;
		visible = pVisible;
		active = pActive;
		img = pImage;
	}
	
	//Going to perform the drawing of the object on the offscreen
	//canvas
	abstract public void draw(Graphics g);
	
	//updateSprite is made for Frogger wrapping, not turning the vehicle enemies instantly around, as that makes no sense
	//this is a mess, no real general usage for updating, Player and Enemy have fully override updateSprite methods
	public void updateSprite()
	{
	
		//if right most then wrap to left side
		if ((getLocX() + 100 > GamePanel.WIDTH_OF_GAMEPANEL_BACKGROUND || getLocX() < 0))
		{
			//old idea of reversing vel
			//setVelX(-1* getVelX());
			
			//new idea of wrapping the enemy - buggy
			//setLocX(0 - 100);
		}
		//if left most then wrap to right side
		/*if (getLocX()<0 ){
			setLocX(GamePanel.WIDTH_OF_GAMEPANEL_BACKGROUND);
		}*/
		
	
		//this if doesnt even matter for this game, could be removed - not incase of future editing
		if (getLocY()>GameFrame.HEIGHT || getLocY()<0 )
		{
			setVelY(-1* getVelY());
		}
		
		//The sopl is good for testing
		//System.out.println("XLoc" + getLocX());
		setLocX(getLocX() + getVelX());
		setLocY(getLocY() + getVelY());
	}
	
	//**************************************************************
	//ACCESSOR AND MUTATOR METHODS
	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getLocX() {
		return locX;
	}

	public void setLocX(int locX) {
		this.locX = locX;
	}

	public int getLocY() {
		return locY;
	}

	public void setLocY(int locY) {
		this.locY = locY;
	}

	public int getVelX() {
		return velX;
	}

	public void setVelX(int velX) {
		this.velX = velX;
	}

	public int getVelY() {
		return velY;
	}

	public void setVelY(int velY) {
		this.velY = velY;
	}
	
	public Image getImage() {
		return img;
	}
	public void setImage(Image pImage) {
		this.img = pImage;
	}
}