
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.net.URL;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/* John VanSickle GamePanel Class
 * This class is the piece that everything graphically interacts as well as how everything 
 * is stored and started/stopped
 */

public class GamePanel extends JPanel implements Runnable {
	
	private static final long serialVersionUID = 1L;
	//vars
	private Image image;
	private String filename;
	private boolean playing;
	private Player player1;
	private Enemy[] enemyVehicles;
	private keyboardGameListener listener;
	private int sleepTime;
	private GameFrame myGameFrameRef;
	private boolean pausedGame;
	private boolean cheatEnabled;
	private int counter;
	private Clip clip;
    private AudioInputStream inputStream;
    	
	public final int NUM_OF_ENEMIES = 6;
	public final int[] Y_COORDS_FOR_TRUCKS = {255, 215, 175, 125, 85, 45};
	public final int[] VELS_FOR_TRUCKS = {10, -10, 15, -18, 20, -23};
	
	public static final int WIDTH_OF_GAMEPANEL_BACKGROUND = 600;
	
	//constructor requires a ref to GameFrame to make a ref var to enable access to the lives 
	//of the GUI (ie: when hit, lives can decrement
	public GamePanel(GameFrame pGF) {
		
		this.setFocusable(true);
		this.setDoubleBuffered(true);
		
		//set cheatENabled to false, as no one should start as a cheater
		cheatEnabled = false;
		
		//set pausedGame to false initially for pause button to work properly
		pausedGame = false;
		
		//get ref for myGameFrameRef
		myGameFrameRef = pGF;
		
		//set default thread sleep time
		setSleepTime(50);
		
		//add the keyboard listener
		listener = new keyboardGameListener();
		this.addKeyListener(listener);
		
		//initialize playing to false so that user must click start button to start the game
		playing = false;
		
		//set background image
		filename = "RoadImageReduced.png";
		this.loadImage();
		this.repaint();
		
		//setup Player1 
		//locX, locY, velX, velY, img, active, visible
		player1 = new Player(300,300,0,0,"Frogger_sprite.png", true, true);
		
		//setup all enemies; 6 enemies to match image
		enemyVehicles = new Enemy[6];
		setupEnemies();
	}

	@Override
	  protected void paintComponent(Graphics g) {
	    super.paintComponent(g);
	    g.drawImage(image, 0, 0, this);
	    
	    Graphics2D g2d = (Graphics2D)g;
	    
	    player1.draw(g2d);
	    

		for (Enemy en : enemyVehicles){
			en.draw(g2d);
		}

	}
	
	//function loadImage to get an image, initially is being used for setting the background image
	public void loadImage() {
    	URL url = null;
    	Toolkit tk = Toolkit.getDefaultToolkit();
        url = this.getClass().getResource(filename);
        image = tk.getImage(url);
    }
	
	//function to set up enemies into enemy array
	private void setupEnemies(){
		//setup enemies such that every other will travel in the opposite direction of the one below it
		for (int i = 0; i < NUM_OF_ENEMIES; i++){			
			if (i % 2 == 0){
				enemyVehicles[i] = new Enemy(0, Y_COORDS_FOR_TRUCKS[i], VELS_FOR_TRUCKS[i], 0, "truckIcon.png", true, true, true);
			}
			else{
				enemyVehicles[i] = new Enemy(500, Y_COORDS_FOR_TRUCKS[i] , VELS_FOR_TRUCKS[i], 0, "truckIcon2.png", true, true, false);
			}
		}
	}//end of setupEnemies


	@Override
	public void run() {
		playGame();
	}
	
	//my function for the thread to run through
	public void playGame(){
		counter = 0;
		
		while (playing){
			this.requestFocus();
			
			//update the user
			player1.updateSprite();
			checkBoundaries();
			
			//go through each enemy vehicle and update it
			for (Enemy en : enemyVehicles){
				en.updateSprite();
			}
			
			//check for collisions
			checkCollisions();
			
			//Sleep for .05 second between drawing so can see the animation
			 try{
				 Thread.sleep(getSleepTime());
			 }
			 catch (InterruptedException exception){
				 System.out.println("Thread exited due to interruption");
			 }
			 
			 counter++;
			 if (counter % 20 == 0 && counter <= 120){
				 for (Enemy en : enemyVehicles){
					 if (en.isDriveRight())
						 en.setVelX(en.getVelX() + 2);
					 else 
						 en.setVelX(en.getVelX() - 2);
				 }
			 }
			 
			 repaint();
		}
	}//end of playGame()
	
	//pauseGame sets playing to false which will stop the playGame method from continueing to execute
	public void pauseGame(){
		playing = false;
	}
	//startGame can be called to start the game or continue it from a paused state
	public void startGame(){
		playing = true;
		
	}
	
	//inner class for e keyboard listener
	//implements using player velocity rather than update X/Y so that movement is instant and constant
	public class keyboardGameListener implements KeyListener{
		@Override
		public void keyPressed(KeyEvent ke) {
			if (playing)
			{
				
				if (ke.getKeyCode() == (KeyEvent.VK_W) || ke.getKeyCode() == (KeyEvent.VK_UP)){
					//new way to move
					player1.setVelY(-10);
					player1.setCurrentAngle(0);
				}
				
				else if (ke.getKeyCode() == KeyEvent.VK_A || ke.getKeyCode() == (KeyEvent.VK_LEFT)){
					//new way to move
					player1.setVelX(-10);
					player1.setCurrentAngle(270);
					
				}
				
				else if (ke.getKeyCode() == KeyEvent.VK_S || ke.getKeyCode() == (KeyEvent.VK_DOWN)){
					//new way to move
					player1.setVelY(10);
					player1.setCurrentAngle(180);
					
				}
				
				else if (ke.getKeyCode() == KeyEvent.VK_D || ke.getKeyCode() == (KeyEvent.VK_RIGHT)){
					//new way to move
					player1.setVelX(10);
					player1.setCurrentAngle(90);
					
				}
			
			}
			
			//outter of the if-playing condition so that the cheat may be enable or disabled at any time
			if(ke.getKeyCode() == KeyEvent.VK_C){
				cheatEnabled = !cheatEnabled;
				
				if (cheatEnabled){
					for (Enemy en : enemyVehicles){
						en.setVelX(0);
					}
				} else {
					int i = 0;
					for (Enemy en : enemyVehicles){
						en.setVelX(VELS_FOR_TRUCKS[i]);
						i++;
					}
				}
			}
			
			//outter of the paused game so that the P key can always be heard
			if(ke.getKeyCode() == KeyEvent.VK_P){
				if (pausedGame == true){
					pausedGame = false;
					playing = true;
					myGameFrameRef.startGamePanelThread();
				}
				else{
					pausedGame = true;
					pauseGame();
					
				}
			}
		}//end of keyPressed Method
		@Override
		public void keyReleased(KeyEvent ke) {
			player1.setVelX(0);
			player1.setVelY(0);
		}//end of keyReleased method
		@Override
		public void keyTyped(KeyEvent ke) {
			
		}
		
	}//end of keyboard listener

	//if collision, reduce life by one and check current lives, if more lives then respawn the player1 at start position (300,300)
	public void checkCollisions()
	{
		for (Enemy en : enemyVehicles){
			if (player1.getBoundingBox().intersects(en.getBoundingBox()))
			{
				//try to play the splat noise when frog is hit
				try {
					clip = AudioSystem.getClip();
					inputStream = AudioSystem.getAudioInputStream(GameFrame.class.getResourceAsStream("/splat.wav"));
				      clip.open(inputStream);
				      clip.start(); 
				    } catch (Exception e) {
				    	//if can't, no need to alert, game works without it
				      //System.err.println(e.getMessage());
				    }
				
				//remove the player right away and "respawn"
				//locX, locY, velX, velY, img, active, visible
				player1 = new Player(300,300,0,0,"Frogger_sprite.png", true, true);
				
				//will decrement the lives by one and update it being displayed
				myGameFrameRef.decLives();
				myGameFrameRef.updateLivesDisplay();
				
				//LOSE condition
				if (myGameFrameRef.getLives() <= 0){
					//stop all movement
					playing = false;
					
					//give a message out that the player lost, close and restart?
					switch(JOptionPane.showConfirmDialog(null, "You Have Lost! Press YES to continue playing or NO to close.", "LOSER!", JOptionPane.YES_NO_OPTION)){
					case 0:
						myGameFrameRef.resetLives();
						myGameFrameRef.updateLivesDisplay();
						myGameFrameRef.enableButtons();
						//redo enemies
						enemyVehicles = new Enemy[6];
						setupEnemies();
						//reset cheatEnabled
						cheatEnabled = false;
						break;
					default:
						System.exit(0);
					}
					
				}
			}
		}
	}
	
	//check boundaries is in GamePanel class only because of the idea that only player1 will be using this
	//and with win conditions the GamePanel needs to communicate with the GameFrame
	//updateSprite() overridden so that is does not interfere with this
	public void checkBoundaries(){
		//win condition here
		if(player1.getLocY() < 0){
			pauseGame();
			
			//if condition for if the player won while cheat enabled
			if (cheatEnabled){
				switch(JOptionPane.showConfirmDialog(null, "You won with cheating... I'm not impressed. Not even a little.\n Press YES to continue playingor NO to close.", "WINNER!", JOptionPane.YES_NO_OPTION)){
				case 0:
					//remove the player right away and "respawn"
					//locX, locY, velX, velY, img, active, visible
					player1 = new Player(300,300,0,0,"Frogger_sprite.png", true, true);
					myGameFrameRef.resetLives();
					myGameFrameRef.updateLivesDisplay();
					myGameFrameRef.enableButtons();
					//redo enemies
					enemyVehicles = new Enemy[6];
					setupEnemies();
					//reset cheatEnabled
					cheatEnabled = false;
					break;
				default:
					System.exit(0);
				}
			}
			//else being that the player won while cheat was not enabled
			else{
				switch(JOptionPane.showConfirmDialog(null, "You Have WON!! Press YES to continue playing or NO to close.", "WINNER!", JOptionPane.YES_NO_OPTION)){
				case 0:
					//remove the player right away and "respawn"
					//locX, locY, velX, velY, img, active, visible
					player1 = new Player(300,300,0,0,"Frogger_sprite.png", true, true);
					myGameFrameRef.resetLives();
					myGameFrameRef.updateLivesDisplay();
					myGameFrameRef.enableButtons();
					//redo enemies
					enemyVehicles = new Enemy[6];
					setupEnemies();
					//reset cheatEnabled
					cheatEnabled = false;
					break;
				default:
					System.exit(0);
				}
			}
		}
		
		else if (player1.getLocX() + player1.image.getWidth(null) < 0){
			player1.setLocX(GamePanel.WIDTH_OF_GAMEPANEL_BACKGROUND);
		}
		
		else if (player1.getLocY() + player1.image.getHeight(null) > image.getHeight(null))
			player1.setLocY(300);
		
		else if ((player1.getLocX() > GamePanel.WIDTH_OF_GAMEPANEL_BACKGROUND)){
			player1.setLocX(0);
		}
	}//end of checkBoundaries
	
	public void setSleepTime(int pNum){
		sleepTime = pNum;
	}
	
	public int getSleepTime(){
		return sleepTime;
	}
}
